#!/bin/bash -u

#### requirements ####
# apt install pdsh logger
# ssh-copy-id on all raspberries
# append raspberries ip in file "all_pi" 

#### on raspberry ####
# visudo / pi   ALL= NOPASSWD: /bin/systemctl poweroff


# ? const variable
scheduler_name="NetworkManager.service" # example
gpio_pin_relay="18"

function get_cc_status() {
    status=$(systemctl status $scheduler_name | grep "Active: active")
    if test "$status" == "";then
        echo "0"
    else
        echo "1"
    fi
}

function set_cc_status() {
    if test $1 -eq 0;then
        # stop scheduler
        systemctl stop $scheduler_name
        # stop raspberry
        pdsh -w ^all_pi -R ssh "systemctl poweroff"
        # stop eternet switch and raspberries power
        echo "0" > /sys/class/gpio/gpio${gpio_pin_relay}/value
    else
        # start scheduler
        systemctl start $scheduler_name
        # start eternet switch and raspberries power
        echo "1" > /sys/class/gpio/gpio${gpio_pin_relay}/value
    fi
}

function get_battery_level() {
    # dev
    echo "1"
    # prod
    # calcul with yocto-watt value and math function
}

batterie=$(get_battery_level)
cc=$(get_cc_status)

if (test $batterie == "1" && test $cc == "1") || (test $batterie == "0" && test $cc == "0");then
    logger -i "nothing to do" -t "cc_m2"
fi

if test $batterie == "1" && test $cc == "0";then
    set_cc_status 1
    logger -i "poweron" -t "cc_m2"
fi

if test $batterie == "0" && test $cc == "1";then
    set_cc_status 0
    logger -i "poweroff" -t "cc_m2"
fi

# cron all minutes
# * * * * * /usr/local/bin/power_manager.sh